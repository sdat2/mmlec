# MMLEC

Miscellaneous Machine Learning Example Code

Examples of using sklearn from the internet that I have refactored to PEP8 style 
and changed to try and understand the syntax (and content) better.

https://scikit-learn.org/stable/modules/gaussian_process.html

Look at 
https://www.robots.ox.ac.uk/~mebden/reports/GPtutorial.pdf

For a quick discussion of Gaussian Proccesses.

This tutorial recommends Sivia and Skilling for a more detailed entropy based discussion.

A thorough exploration of Gaussian processes is available here:
http://www.gaussianprocess.org/gpml/chapters/RW.pdf

Information Theory, Inference and Learning Algorithms (ITILA) is as always the reference text.
