"""
==========================================================================
Illustration of prior and posterior Gaussian process for different kernels
==========================================================================

This example illustrates the prior and posterior of a GPR with different
kernels. Mean, standard deviation, and 10 samples are shown for both prior
and posterior.

# Authors: Jan Hendrik Metzen <jhm@informatik.uni-bremen.de>
#
# License: BSD 3 clause

"""
import numpy as np
from matplotlib import pyplot as plt
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import (RBF, Matern, RationalQuadratic,
                                              ExpSineSquared, DotProduct,
                                              ConstantKernel)
from utilities import my_plotting_style as mps
from utilities import my_io_functions as mif
mps.mps_defaults(quality='high')


def kernel_comparison_function(kernels, kernel_file_label):
    """
    
    :param kernels: 
    :param kernel_file_label: 
    :param kernel_plot_title: 
    :return: 
    """
    for kernel_no in range(len(kernels)):
        # Specify Gaussian Process
        gp_object = GaussianProcessRegressor(kernel=kernels[kernel_no])
        # Plot prior
        plt.figure(figsize=(8, 8))
        plt.subplot(2, 1, 1)
        x_npa_20pu = np.linspace(0, 5, 100)
        y_pred_npa_20pu, y_std = gp_object.predict(x_npa_20pu[:, np.newaxis], return_std=True)
        plt.plot(x_npa_20pu, y_pred_npa_20pu, 'k', lw=3, zorder=9)
        plt.fill_between(x_npa_20pu, y_pred_npa_20pu - y_std, y_pred_npa_20pu + y_std,
                         alpha=0.2, color='k')
        y_samples = gp_object.sample_y(x_npa_20pu[:, np.newaxis], 10)
        plt.plot(x_npa_20pu, y_samples, lw=1)
        plt.xlim(0, 5)
        plt.ylim(-3, 3)
        # seems to brake here:
        plt.title(mif.tex_escape("Prior (kernel: %s)" % gp_object.kernel))
        # Generate data and fit GP
        rng = np.random.RandomState(4)
        x_npa_2pu = rng.uniform(0, 5, 10)[:, np.newaxis]
        y_samples_2pu = np.sin((x_npa_2pu[:, 0] - 2.5) ** 2)
        gp_object.fit(x_npa_2pu, y_samples_2pu)
        # Plot posterior
        plt.subplot(2, 1, 2)
        x_npa_20pu = np.linspace(0, 5, 100)
        y_pred_npa_20pu, y_std = gp_object.predict(x_npa_20pu[:, np.newaxis], return_std=True)
        plt.plot(x_npa_20pu, y_pred_npa_20pu, 'k', lw=3, zorder=9)
        # plot the envelope
        plt.fill_between(x_npa_20pu, y_pred_npa_20pu - y_std, y_pred_npa_20pu + y_std,
                         alpha=0.2, color='k')
        y_samples = gp_object.sample_y(x_npa_20pu[:, np.newaxis], 10)
        plt.plot(x_npa_20pu, y_samples, lw=1)
        plt.scatter(x_npa_2pu[:, 0], y_samples_2pu, c='r', s=50, zorder=10, edgecolors=(0, 0, 0))
        plt.xlim(0, 5)
        plt.ylim(-3, 3)
        plt.title(mif.tex_escape("Posterior (kernel: %s)\n Log-Likelihood: %.3f"
                                 % (gp_object.kernel_,
                                    gp_object.log_marginal_likelihood(gp_object.kernel_.theta))),
                  fontsize=12)
        # plt.title("Log-Likelihood: %.3f"
        #           % gp_object.log_marginal_likelihood(gp_object.kernel_.theta))
        print('kernel number: ', kernel_no)
        plt.title(mif.tex_escape("Posterior (kernel: %s)\n Log-Likelihood: %.3f"
                                 % (gp_object.kernel_,
                                    gp_object.log_marginal_likelihood(gp_object.kernel_.theta))))
        fig = plt.gcf()
        fig = mps.defined_size(fig, size='double_column_square')
        plt.tight_layout()
        plt.savefig('plots/Kernel_Choice_' + kernel_file_label[kernel_no] + '.pdf',
                    bbox_inches='tight')
        plt.clf()


def run_kernel_comparison_function():
    """

    :return:
    """
    # set up kernels with reasonable parameters
    kernels = [1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),
               1.0 * RationalQuadratic(length_scale=1.0, alpha=0.1),
               1.0 * ExpSineSquared(length_scale=1.0, periodicity=3.0,
                                    length_scale_bounds=(0.1, 10.0),
                                    periodicity_bounds=(1.0, 10.0)
                                    ),
               ConstantKernel(0.1, (0.01, 10.0))
               * (DotProduct(sigma_0=1.0, sigma_0_bounds=(0.1, 10.0)) ** 2),
               1.0 * Matern(length_scale=1.0, length_scale_bounds=(1e-1, 10.0), nu=1.5)
               ]

    kernel_file_label = ['Radial_Basis_Function',
                         'Rational_Quadratic',
                         'Exp_Sine_Squared',
                         'Dot_Product',
                         'Matern'
                         ]

    kernel_comparison_function(kernels, kernel_file_label)


run_kernel_comparison_function()
