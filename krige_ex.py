"""
=========================================================
Gaussian Processes regression: basic introductory example
=========================================================

A simple one-dimensional regression example computed in two different ways:

1. A noise-free case
2. A noisy case with known noise-level per datapoint

In both cases, the kernel's parameters are estimated using the maximum
likelihood principle.

The figures illustrate the interpolating property of the Gaussian Process
model as well as its probabilistic nature in the form of a pointwise 95%
confidence interval.

Note that the parameter ``alpha`` is applied as a Tikhonov
regularization of the assumed covariance between the training points.

# Author: Vincent Dubourg <vincent.dubourg@gmail.com>
#         Jake Vanderplas <vanderplas@astro.washington.edu>
#         Jan Hendrik Metzen <jhm@informatik.uni-bremen.de>s
# License: BSD 3 clause

The program uses:
kernel = C(1.0, (1e-3, 1e3)) * RBF(10, (1e-2, 1e2))
and
gp = GaussianProcessRegressor(kernel=kernel,
                              n_restarts_optimizer=9)

gp = GaussianProcessRegressor(kernel=kernel,
                              alpha=dy ** 2,
                              n_restarts_optimizer=10)

"""

import numpy as np
from matplotlib import pyplot as plt
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
from utilities import my_io_functions as mif
from utilities import my_plotting_style as mps
mps.mps_defaults(quality='high')
np.random.seed(1)


def _funct(x_input):
    """The function to predict."""
    return x_input * np.sin(x_input)


def noiseless_case():
    """
    First the noiseless case.
    :return: void
    """
    x_samples_npa = np.atleast_2d([1., 3., 5., 6., 7., 8.]).T
    # Observations
    y_samples_npa = _funct(x_samples_npa).ravel()
    # Mesh the input space for evaluations of the real function, the prediction and
    #  its MSE
    x_continuous_npa = np.atleast_2d(np.linspace(0, 10, 1000)).T
    # Instantiate a Gaussian Process model
    kernel = C(1.0, (1e-3, 1e3)) * RBF(10, (1e-2, 1e2))
    gp_object = GaussianProcessRegressor(kernel=kernel,
                                         n_restarts_optimizer=9)
    # Fit to data using Maximum Likelihood Estimation of the parameters
    gp_object.fit(x_samples_npa, y_samples_npa)
    # Make the prediction on the meshed x-axis (ask for MSE as well)
    y_pred, sigma = gp_object.predict(x_continuous_npa, return_std=True)
    # Plot the function, the prediction and the 95% confidence interval based on
    #  the MSE
    # plt.figure()
    # plot the function that was used to generate the data set
    plt.plot(x_continuous_npa, _funct(x_continuous_npa), 'r:', label=r'$f(x) = x\,\sin(x)$')
    # plot the data set that was generated
    plt.plot(x_samples_npa, y_samples_npa, 'r.', markersize=10, label='Observations')
    # Plot the prediction of the Gaussian process
    plt.plot(x_continuous_npa, y_pred, 'b-', label='Prediction')
    # plot Gaussian Process envelope
    plt.fill(np.concatenate([x_continuous_npa, x_continuous_npa[::-1]]),
             np.concatenate([y_pred - 1.9600 * sigma,
                             (y_pred + 1.9600 * sigma)[::-1]]),
             alpha=0.5, fc='b', ec='None', label=r'95\% confidence interval')

    # plotting features.
    plt.title(mif.tex_escape("Posterior (kernel: %s)\n Log-Likelihood: %.3f"
                             % (gp_object.kernel_,
                                gp_object.log_marginal_likelihood(gp_object.kernel_.theta))),
              fontsize=12)
    plt.xlabel('$x$')
    plt.ylabel('$f(x)$')
    plt.ylim(-10, 15)
    plt.xlim([0, 10])
    plt.legend(loc='upper left')
    plt.savefig('plots/Noiseless_Case.pdf')
    plt.clf()


def noisy_case():
    """
    now the noisy case
    :return: void
    """
    x_samples_npa = np.linspace(0.1, 9.9, 20)
    x_samples_npa = np.atleast_2d(x_samples_npa).T
    x_npa = np.atleast_2d(np.linspace(0, 10, 1000)).T

    # Observations and noise
    y_samples_npa = _funct(x_samples_npa).ravel()
    # create error bars
    y_sample_error_npa = 0.5 + 1.0 * np.random.random(y_samples_npa.shape)
    # create the noise
    noise = np.random.normal(0, y_sample_error_npa)
    # add the noise to the observations
    y_samples_npa += noise
    # Instantiate a Gaussian Process model
    # Create the RBF Kernel
    kernel = C(1.0, (1e-3, 1e3)) * RBF(10, (1e-2, 1e2))
    gp_object = GaussianProcessRegressor(kernel=kernel,
                                         alpha=y_sample_error_npa ** 2,
                                         n_restarts_optimizer=10)
    # Fit to data using Maximum Likelihood Estimation of the parameters
    gp_object.fit(x_samples_npa, y_samples_npa)
    # Make the prediction on the meshed x-axis (ask for MSE as well)
    y_pred, sigma = gp_object.predict(x_npa, return_std=True)
    # Plot the function, the prediction and the 95% confidence interval based on
    # the MSE

    # plt.figure()
    plt.plot(x_npa, _funct(x_npa), 'r:', label=r'$f(x) = x\,\sin(x)$')
    # plot with error bars.
    plt.errorbar(x_samples_npa.ravel(), y_samples_npa, y_sample_error_npa,
                 fmt='r.', markersize=10, label='Observations')
    # plot the prediction
    plt.plot(x_npa, y_pred, 'b-', label='Prediction')
    # plot the Gaussian process envelope
    plt.fill(np.concatenate([x_npa, x_npa[::-1]]),
             np.concatenate([y_pred - 1.9600 * sigma,
                             (y_pred + 1.9600 * sigma)[::-1]]),
             alpha=0.5, fc='b', ec='None', label=r'95\% confidence interval')

    # plotting features.
    plt.title(mif.tex_escape("Posterior (kernel: %s)\n Log-Likelihood: %.3f"
                             % (gp_object.kernel_,
                                gp_object.log_marginal_likelihood(gp_object.kernel_.theta))),
              fontsize=12)
    plt.xlabel('$x$')
    plt.ylabel('$f(x)$')
    plt.ylim([-10, 15])
    plt.xlim([0, 10])
    plt.legend(loc='upper left')
    plt.savefig('plots/Noisy_Case.pdf')


noiseless_case()
noisy_case()
