"""
=============================================================
Gaussian process regression (GPR) with noise-level estimation
=============================================================

This example illustrates that GPR with a sum-kernel including a WhiteKernel can
estimate the noise level of data. An illustration of the
log-marginal-likelihood (lml) landscape shows that there exist two local
maxima of lml. The first corresponds to a model with a high noise level and a
large length scale, which explains all variations in the data by noise. The
second one has a smaller noise level and shorter length scale, which explains
most of the variation by the noise-free functional relationship. The second
model has a higher likelihood; however, depending on the initial value for the
hyperparameters, the gradient-based optimization might also converge to the
high-noise solution. Thererfore it is important to repeat the optimization several
times for different initializations.

# Authors: Jan Hendrik Metzen <jhm@informatik.uni-bremen.de>
#
# License: BSD 3 clause
"""

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, WhiteKernel
from utilities import my_io_functions as mif
from utilities import my_plotting_style as mps
mps.mps_defaults(quality='high')


def first_run_through(x_npa_5pu, y_npa_5pu):
    """

    :return:
    """
    # Initially use a kernel with a high noise level of 1
    kernel = (1.0 * RBF(length_scale=100.0, length_scale_bounds=(1e-2, 1e3))
              + WhiteKernel(noise_level=1, noise_level_bounds=(1e-10, 1e+1)))
    # set up Gaussian process object
    gp_object = GaussianProcessRegressor(kernel=kernel,
                                         alpha=0.0).fit(x_npa_5pu, y_npa_5pu)
    x_array_20pu = np.linspace(0, 5, 100)
    y_pred_20pu, y_cov = gp_object.predict(x_array_20pu[:, np.newaxis], return_cov=True)
    plt.plot(x_array_20pu, y_pred_20pu, 'k', lw=3, zorder=9)
    plt.fill_between(x_array_20pu, y_pred_20pu - np.sqrt(np.diag(y_cov)),
                     y_pred_20pu + np.sqrt(np.diag(y_cov)),
                     alpha=0.5, color='k')
    plt.plot(x_array_20pu, 0.5*np.sin(3*x_array_20pu), 'r', lw=3, zorder=9)
    plt.scatter(x_npa_5pu[:, 0], y_npa_5pu, c='r', s=50, zorder=10, edgecolors=(0, 0, 0))
    plt.title(mif.tex_escape("Initial: %s\nOptimum: %s\nLog-Marginal-Likelihood: %s"
                             % (kernel, gp_object.kernel_,
                                gp_object.log_marginal_likelihood(gp_object.kernel_.theta))))
    plt.tight_layout()
    plt.savefig('plots/high_noise_version' + '.pdf', bbox_inches='tight')
    plt.clf()


def second_run_through(x_npa_5pu, y_npa_5pu):
    """

    :return:
    """
    # look, we're using a different kernel
    kernel = (1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-2, 1e3))
              + WhiteKernel(noise_level=1e-5, noise_level_bounds=(1e-10, 1e+1)))

    gp_object = GaussianProcessRegressor(kernel=kernel, alpha=0.0).fit(x_npa_5pu, y_npa_5pu)
    x_array_20pu = np.linspace(0, 5, 100)
    y_pred_20pu, y_cov = gp_object.predict(x_array_20pu[:, np.newaxis], return_cov=True)
    plt.plot(x_array_20pu, y_pred_20pu, 'k', lw=3, zorder=9)
    plt.fill_between(x_array_20pu, y_pred_20pu - np.sqrt(np.diag(y_cov)),
                     y_pred_20pu + np.sqrt(np.diag(y_cov)),
                     alpha=0.5, color='k')
    plt.plot(x_array_20pu, 0.5*np.sin(3*x_array_20pu), 'r', lw=3, zorder=9)
    plt.scatter(x_npa_5pu[:, 0], y_npa_5pu, c='r', s=50, zorder=10, edgecolors=(0, 0, 0))
    plt.title(mif.tex_escape("Initial: %s\nOptimum: %s\nLog-Marginal-Likelihood: %s"
                             % (kernel, gp_object.kernel_,
                                gp_object.log_marginal_likelihood(gp_object.kernel_.theta))))
    plt.tight_layout()
    plt.savefig('plots/low_noise_version' + '.pdf', bbox_inches='tight')
    plt.clf()


def plot_lml_landscape(x_npa_5pu, y_npa_5pu):
    """

    :return:
    """
    kernel = (1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-2, 1e3))
              + WhiteKernel(noise_level=1e-5, noise_level_bounds=(1e-10, 1e+1))
              )

    gp_object = GaussianProcessRegressor(kernel=kernel,
                                         alpha=0.0).fit(x_npa_5pu, y_npa_5pu)

    theta0_npa = np.logspace(-2, 3, 49)
    theta1_npa = np.logspace(-2, 0, 50)
    theta0_npm, theta1_npm = np.meshgrid(theta0_npa, theta1_npa)
    # this is where all of lml is creating. Hacky.
    lml = [[gp_object.log_marginal_likelihood(np.log([0.36, theta0_npm[i, j], theta1_npm[i, j]]))
            for i in range(theta0_npm.shape[0])] for j in range(theta0_npm.shape[1])]
    lml = np.array(lml).T
    vmin, vmax = (-lml).min(), (-lml).max()
    vmax = 50  # why do they do
    level = np.around(np.logspace(np.log10(vmin), np.log10(vmax), 50), decimals=1)
    plt.contour(theta0_npm, theta1_npm, -lml,
                levels=level, norm=LogNorm(vmin=vmin, vmax=vmax))
    plt.colorbar()
    plt.xscale("log")
    plt.yscale("log")
    plt.xlabel("Length-scale")
    plt.ylabel("Noise-level")
    plt.title("Log-marginal-likelihood")
    plt.tight_layout()
    plt.savefig('plots/LML_landscape' + '.pdf', bbox_inches='tight')
    plt.clf()


def run_all():
    """

    :return:
    """
    rng = np.random.RandomState(0)  # passing seed in
    x_npa_5pu = rng.uniform(0, 5, 20)[:, np.newaxis]
    y_npa_5pu = 0.5 * np.sin(3 * x_npa_5pu[:, 0]) + rng.normal(0, 0.5, x_npa_5pu.shape[0])
    first_run_through(x_npa_5pu, y_npa_5pu)
    second_run_through(x_npa_5pu, y_npa_5pu)
    plot_lml_landscape(x_npa_5pu, y_npa_5pu)


run_all()
# Gaussian processes feel like the progression from a fourier series to 
# A Fourier transform.
