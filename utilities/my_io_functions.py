"""
A file to contain output protocol
TODO create a function given a set of inputs which will output a definite
TODO file name

"""
import re
import json
import codecs
import pickle
import csv
from utilities import my_wrappers as mwr


@mwr.timeit
def read_json_object(file_name):
    """
    :param file_name:
    :return: json_object: unknown json object
    """
    with codecs.open(file_name, 'rb', encoding='utf-8') as handle:
        json_object = json.load(handle)
    return json_object


@mwr.timeit
def write_json_object(json_object, file_name):
    """
    :param json_object:
    :param file_name:
    :return: void
    """
    with codecs.open(file_name, 'w', encoding='utf-8') as handle:
        json.dump(json_object, handle, separators=(',', ':'), sort_keys=True, indent=4)


@mwr.timeit
def write_pickle_object(pickle_object, file_name):
    """
    :param pickle_object:
    :param file_name:
    :return: void
    """
    with open(file_name, 'wb') as handle:
        pickle.dump(pickle_object, handle, protocol=pickle.HIGHEST_PROTOCOL)


@mwr.timeit
def write_csv_file(list_of_lists, file_name):
    """
    :param list_of_lists: a list of the rows to be saved
    :param file_name: A valid file name (including path)
    :return: void
    https://www.programiz.com/python-programming/working-csv-files
    """
    with open(file_name, 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(list_of_lists)


def tex_escape(text):
    """
    It is better to plot in TeX, but involves escaping strings.
    from:
    https://stackoverflow.com/questions/16259923/
    how-can-i-escape-latex-special-characters-inside-django-templates
        :param text: a plain text message
        :return: the message escaped to appear correctly in LaTeX
    # removed unicode(key) from re.escape because this seemed an unnecessary,
      and was throwing an error.
    """
    conv = {
            '&': r'\&',
            '%': r'\%',
            '$': r'\$',
            '#': r'\#',
            '_': r'\_',
            '{': r'\{',
            '}': r'\}',
            '~': r'\textasciitilde{}',
            '^': r'\^{}',
            '\\': r'\textbackslash{}',
            '<': r'\textless{}',
            '>': r'\textgreater{}',
            }
    regex = re.compile('|'.join(re.escape(key) for key in sorted(conv.keys(), key=lambda item: - len(item))))
    return regex.sub(lambda match: conv[match.group()], text)
