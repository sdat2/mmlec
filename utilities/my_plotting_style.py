"""
This should improve the graphics in the document by using TeX for text,
(if high chosen) using the same font as the report, and using reasonable text sizes.
Usage:
from my_plotting_style import mps
mps.mps_defaults('high) # if you want to engage TeX.
Incurs a large time penalty, worth it only for final plots.
Blank entries should cause plots to inherit fonts from the document.
"""

import matplotlib


def mps_defaults(quality='high'):
    """
    Apply my plotting style to produce nice looking figures.
    Call this at the start of a script which uses matplotlib,
    and choose the correct setting.
    :return:
    """
    if quality == 'high':
        p_setting = {"pgf.texsystem": "pdflatex",
                     "text.usetex": True,
                     "font.family": "serif",
                     "font.serif": [],
                     "font.sans-serif": ["DejaVu Sans"],    # gets rid of error messages
                     "font.monospace": [],
                     "lines.linewidth": 0.75,
                     "axes.labelsize": 12,
                     "font.size": 10,
                     "legend.fontsize": 8,
                     "xtick.labelsize": 10,
                     "ytick.labelsize": 10,
                     "image.cmap": 'RdYlBu_r',
                     # I think that this is a good temperature cmap
                     "animation.html": 'none',
                     "animation.writer": 'ffmpeg',          # MovieWriter 'backend' to use
                     "animation.codec": 'h264',             # Codec to use for writing movie
                     "animation.bitrate": -1,               # -1 implies let utility auto-determine
                     # Controls size/quality trade off for movie.
                     "animation.frame_format": 'png',
                     # Controls frame format used by temp files
                     "animation.ffmpeg_path": 'ffmpeg',
                     # Path to ffmpeg binary. Without full path $PATH is searched
                     "animation.ffmpeg_args": '',           # Additional arguments to pass to ffmpeg
                     "animation.avconv_path": 'avconv',
                     # Path to avconv binary. Without full path $PATH is searched
                     "animation.avconv_args": '',           # Additional arguments to pass to avconv
                     "pgf.preamble": [r"\usepackage[utf8x]{inputenc}",
                                      r"\usepackage[T1]{fontenc}",
                                      r"\usepackage[separate -uncertainty=true]{siunitx}",
                                      ]
                     }
    else:
        p_setting = {"text.usetex": False,
                     "lines.linewidth": 0.75,
                     "axes.labelsize": 10,
                     "font.size": 6,
                     "legend.fontsize": 8,
                     "xtick.labelsize": 10,
                     "ytick.labelsize": 10,
                     "image.cmap": 'RdYlBu_r',
                     }

    matplotlib.rcParams.update(p_setting)


def single_column_width(fig):
    """Legacy function"""
    fig = defined_size(fig, size='single_column')
    return fig


def defined_size(fig, size='single_column'):
    """
    #---text sizes are meaningless if LaTeX squashes figure---
    usage:
    fig = plt.gcf()
    fig = defined_size(fig, size='double_column_square')
    plt.tight_layout()
    ######
    Sizes are in inches by default.
    Measurements taken by ruler from a report.
    :param fig: the figure to be resized
    :param size: key word argument to get the right
    :param kwargs: unnecesary.
    :return: figure with the correct sized boundary
    """
    size_dict = {'single_column': [3.125, 2.5],
                 'sc': [3.125, 2.5],
                 'scs': [3.125, 3.125],
                 'double_column_short': [6.5, 2.5],
                 'dcsh': [6.5, 2.5],
                 'double_column_square': [6.5, 6.5],
                 'double_column_medium': [6.5, 4.5],
                 'dcsq': [6.5, 6.5],
                 'gal_an_size': [7, 6.5]}
    fig.set_size_inches(size_dict[size])
    return fig
