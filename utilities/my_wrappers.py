#!/usr/bin/env python3
"""
An attempt to put all my useful wrappers in the same place
see
https://wiki.python.org/moin/PythonDecoratorLibrary
"""
import os.path  # for memorise
import json # for memorise
import codecs # for json for memorise
import time
import collections # for memorise
import inspect # for memorise
import functools  # used by both
from functools import wraps
import re
import unicodedata


def timeit(method):
    """
    timeit is a wrapper for performance analysis which should
    return the time taken for a function to run,
    :param method: the function that it takes as an input
    :return: timed
    example usage:
    tmp_log_data={}
    part = spin_forward(400, co, particles=copy.deepcopy(particles),
                        log_time=tmp_log_data) # chuck it into part to stop interference.
    assert part != particles
    spin_round_time[key].append(tmp_log_data['SPIN_FORWARD'])
    TODO make this function user friendly for getting the data from.
    """
    @wraps(method)
    def timed(*args, **kw):
        ts = time.perf_counter()
        result = method(*args, **kw)
        te = time.perf_counter()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = ((te - ts))
        else:
            print('%r  %2.5f s\n' % (method.__name__, (te - ts)))
        return result
    return timed


class Memorise(object):
    """
    Originally from
    https://github.com/brmscheiner/memorize.py/blob/master/memorize/memorize.py
    A function decorated with @Memorize caches its return
    value every time it is called. If the function is called
    later with the same arguments, the cached value is
    returned (the function is not reevaluated). The cache is
    stored as a .json file in the current directory for reuse
    in future executions. If the Python file containing the
    decorated function has been updated since the last run,
    the current cache is deleted and a new cache is created
    (in case the behavior of the function has changed).
    TODO allow wrapper to handle functions with non-hashable arguments
    def getHash(arg1, arg2):
        return str([arg1, arg2])
    @Memorize(getHash)
    def myFunction(arg1, arg2):
        ....
     check getHash when Memorize is initialized?
     And recreate the cache file if it has changed?
     we will need to save a hash of the function to the cache as well
     , so that when myFunction is called again we can
      verify that the getHash function has not changed.
    TODO work out where the caches are going after run time
    TODO stop the program running twice because of unhashable variables
    TODO test memorise.
    """

    # This configures the place to store cache files globally.
    # Set it to False to store cache files next to files for which function calls are cached.
    USE_CURRENT_DIR = True

    def __init__(self, func):
        self.func = func
        function_file = inspect.getfile(func)
        self.parent_filepath = os.path.abspath(function_file)
        self.parent_filename = os.path.basename(function_file)
        self.__name__ = self.func.__name__
        self.cache = None  # lazily initialize cache to account for changed global dir setting (USE_CURRENT_DIR)
        self.unhashable_no = 0

    def check_cache(self):
        if self.cache is None:
            if self.cache_exists():
                self.read_cache()  # Sets self.timestamp and self.cache
                if not self.is_safe_cache():
                    self.cache = {}
            else:
                self.cache = {}

    def __call__(self, *args):
        self.check_cache()
        try:
            if args in self.cache:
                return self.cache[args]
            else:
                value = self.func(*args)
                self.cache[args] = value
                self.save_cache()
                return value
        except TypeError:  # unhashable arguments
            value = self.func(*args)
            self.cache[self.unhash_no] = value
            self.unhashable_no += 1
            return self.func(*args)

    def get_cache_filename(self):
        """
        Sets self.cache_filename to an os-compliant
        version of "file_function.cache"
        """
        filename = _slugify(self.parent_filename.replace('.py', ''))
        funcname = _slugify(self.__name__)
        folder = os.path.curdir if self.USE_CURRENT_DIR else os.path.dirname(self.parent_filepath)
        return os.path.join(folder, filename + '_' + funcname + '.json')

    def get_last_update(self):
        """
        Returns the time that the parent file was last
        updated.
        """
        last_update = os.path.getmtime(self.parent_filepath)
        return last_update

    def is_safe_cache(self):
        """
        Returns True if the file containing the memoized
        function has not been updated since the cache was
        last saved.
        """
        if self.get_last_update() > self.timestamp:
            return False
        return True

    def read_cache(self):
        """
        Read a pickled dictionary into self.timestamp and
        self.cache. See self.save_cache.
        """
        with codecs.open(self.get_cache_filename(), 'rb', encoding='utf-8') as handle:
            data = json.load(handle)
            self.timestamp = data['timestamp']
            self.cache = data['cache']

    def save_cache(self):
        """
        Pickle the file's timestamp and the function's cache
        in a dictionary object.
        """

        with codecs.open(self.get_cache_filename(), 'wb+', encoding='utf-8') as handle:
            out = dict()
            out['timestamp'] = self.get_last_update()
            out['cache'] = self.cache
            json.dump(out, handle, separators=(',', ':'), sort_keys=True, indent=4)

    def cache_exists(self):
        """
        Returns True if a matching cache exists in the current directory.
        """
        if os.path.isfile(self.get_cache_filename()):
            return True
        return False

    def __repr__(self):
        """ Return the function's docstring. """
        return self.func.__doc__

    def __get__(self, obj, objtype):
        """ Support instance methods. """
        return functools.partial(self.__call__, obj)


def _slugify(value):
    """
    Normalizes string, converts to lowercase, removes
    non-alpha characters, and converts spaces to
    hyphens. From
    http://stackoverflow.com/questions/295135/turn-a-string-into-a-valid-filename-in-python
    """
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = re.sub(r'[^\w\s-]', '', value.decode('utf-8', 'ignore'))
    value = value.strip().lower()
    value = re.sub(r'[-\s]+', '-', value)
    return value
